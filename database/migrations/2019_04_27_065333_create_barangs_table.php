<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barangs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode')->nullable();
            $table->string('nama');
            $table->string('deskripsi');
            $table->string('jenis');
            $table->string('tipe');
            $table->string('lokasi');
            $table->string('ruang');
            $table->string('tahun');
            $table->string('pengadaan');
            $table->string('nomor');
            $table->string('status');
            $table->string('gambar');
            $table->string('user');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barang');
    }
}
