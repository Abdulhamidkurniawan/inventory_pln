-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 20, 2019 at 11:40 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory_pln`
--
CREATE DATABASE IF NOT EXISTS `inventory_pln` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `inventory_pln`;

-- --------------------------------------------------------

--
-- Table structure for table `barangs`
--

CREATE TABLE `barangs` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci,
  `jenis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipe` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lokasi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ruang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `koordinat` text COLLATE utf8mb4_unicode_ci,
  `tahun` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pengadaan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `barangs`
--

INSERT INTO `barangs` (`id`, `kode`, `nama`, `deskripsi`, `jenis`, `tipe`, `lokasi`, `ruang`, `koordinat`, `tahun`, `pengadaan`, `nomor`, `status`, `gambar`, `user`, `created_at`, `updated_at`) VALUES
(5, 'KRA.01.07.0002', 'Logitech g102', 'adadada', 'Elektronik', 'Mouse', 'KRA', 'Satpam', '-0.496548, 117.143926', '2019', 'PO', '0123', 'Baik', '40055168.jpg', 'wawan', '2019-05-05 19:58:45', '2019-06-27 11:00:29');

-- --------------------------------------------------------

--
-- Table structure for table `jenis`
--

CREATE TABLE `jenis` (
  `id` int(10) UNSIGNED NOT NULL,
  `jenis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jenis`
--

INSERT INTO `jenis` (`id`, `jenis`, `kode`, `created_at`, `updated_at`) VALUES
(1, 'Elektronik', '01', '2019-04-27 08:30:39', '2019-04-27 08:32:16'),
(2, 'Furniture', '02', '2019-04-27 08:30:39', '2019-04-27 08:32:16'),
(3, 'Alat Kebersihan', '03', '2019-04-27 09:41:02', '2019-04-27 09:41:02'),
(4, 'Multimedia', '04', '2019-04-27 09:41:17', '2019-04-27 09:41:17'),
(5, 'Alat Makan', '05', '2019-04-27 09:41:30', '2019-04-27 09:41:36');

-- --------------------------------------------------------

--
-- Table structure for table `kantors`
--

CREATE TABLE `kantors` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `singkatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kantors`
--

INSERT INTO `kantors` (`id`, `nama`, `singkatan`, `kode`, `created_at`, `updated_at`) VALUES
(1, 'Kantor Mahakam', 'MHK', '01', '2019-04-27 08:26:07', '2019-04-27 08:26:07'),
(2, 'PLTD Karang Asam', 'KRA', '02', '2019-04-28 06:47:53', '2019-04-28 06:47:53');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_04_27_065333_create_barangs_table', 1),
(4, '2019_04_27_142412_create_kantors_table', 1),
(5, '2019_04_27_142608_create_jeniss_table', 1),
(6, '2019_04_27_142640_create_tipes_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pengadaans`
--

CREATE TABLE `pengadaans` (
  `id` int(10) UNSIGNED NOT NULL,
  `jenis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pengadaans`
--

INSERT INTO `pengadaans` (`id`, `jenis`, `kode`, `created_at`, `updated_at`) VALUES
(6, 'PO', 'PO', '2019-06-15 19:07:09', '2019-06-15 19:07:09'),
(8, 'Non PO', 'Non PO', '2019-06-15 19:08:23', '2019-06-15 19:08:23');

-- --------------------------------------------------------

--
-- Table structure for table `tipes`
--

CREATE TABLE `tipes` (
  `id` int(10) UNSIGNED NOT NULL,
  `tipe` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tipes`
--

INSERT INTO `tipes` (`id`, `tipe`, `jenis`, `kode`, `created_at`, `updated_at`) VALUES
(1, 'Laptop', 'Elektronik', '01', '2019-04-27 09:06:20', '2019-04-27 09:21:04'),
(2, 'All in One PC', 'Elektronik', '02', '2019-04-29 16:00:00', '2019-04-29 16:00:00'),
(3, 'Monitor PC', 'Elektronik', '03', '2019-04-29 08:42:48', '2019-04-29 08:42:48'),
(4, 'CPU', 'Elektronik', '04', '2019-04-29 08:43:05', '2019-04-29 08:43:05'),
(5, 'Printer', 'Elektronik', '05', '2019-04-29 08:43:14', '2019-04-29 08:43:41'),
(6, 'Speaker', 'Elektronik', '06', '2019-04-29 08:43:34', '2019-04-29 08:43:47'),
(7, 'Mouse', 'Elektronik', '07', '2019-04-29 08:44:23', '2019-04-29 08:44:23'),
(8, 'LCD Projector', 'Elektronik', '08', '2019-04-29 08:44:40', '2019-04-29 08:44:40'),
(9, 'Televisi', 'Elektronik', '09', '2019-04-29 08:44:52', '2019-04-29 08:44:52'),
(10, 'Scanner', 'Elektronik', '10', '2019-04-29 08:45:09', '2019-04-29 08:45:09'),
(11, 'AC', 'Elektronik', '11', '2019-04-29 08:45:23', '2019-04-29 08:45:23'),
(12, 'Kulkas', 'Elektronik', '12', '2019-04-29 08:45:38', '2019-04-29 08:45:38'),
(13, 'DVR', 'Elektronik', '13', '2019-04-29 08:45:48', '2019-04-29 08:45:48'),
(14, 'Antena', 'Elektronik', '14', '2019-04-29 08:45:55', '2019-04-29 08:45:55'),
(15, 'Radio Accesspoint', 'Elektronik', '15', '2019-04-29 08:46:10', '2019-04-29 08:46:10'),
(16, 'Patrol Guard', 'Elektronik', '16', '2019-04-29 08:46:21', '2019-04-29 08:46:21'),
(17, 'CCTV', 'Elektronik', '17', '2019-04-29 08:46:30', '2019-04-29 08:46:30'),
(18, 'Meja', 'Furniture', '01', '2019-04-29 08:46:57', '2019-04-29 08:46:57'),
(19, 'Kursi', 'Furniture', '02', '2019-04-29 08:47:09', '2019-04-29 08:47:09');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `jabatan`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'wawan', 'wawan@gmail.com', 'admin', NULL, '$2y$10$/ZncElWocc7N8YuL6XKVwuicTYwzycMinIgTV76Yg2JKqEE6Uwayy', 'clSMeDEFmRPYwMi9MClrkgKBWTaCgr6oh31w92jY5V5NmgYEfLv8PKlaVrJB', '2019-04-27 08:25:51', '2019-04-30 22:33:35'),
(2, 'reza', 'reza@gmail.com', 'karyawan', NULL, '$2y$10$3rUYJ644aWgu.SPBYP5mjOtH4SK2ixS4sn8vrCRm0HcT00QT6CFvy', 'Ph3PsAH21pVSoromHeaVCfVKqyBUWLQhsEHQwqsZe4cYrTG2FCsMCBFBzT5O', '2019-05-01 07:20:13', '2019-06-24 16:10:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barangs`
--
ALTER TABLE `barangs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kantors`
--
ALTER TABLE `kantors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pengadaans`
--
ALTER TABLE `pengadaans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipes`
--
ALTER TABLE `tipes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barangs`
--
ALTER TABLE `barangs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `jenis`
--
ALTER TABLE `jenis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kantors`
--
ALTER TABLE `kantors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pengadaans`
--
ALTER TABLE `pengadaans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tipes`
--
ALTER TABLE `tipes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
