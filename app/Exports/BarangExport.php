<?php

namespace App\Exports;

use App\Barang;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class BarangExport implements FromView
{
    use Exportable;

    public function view(): View
    {
        return view('barang.excel', [
            'barangs' => Barang::all()
        ]);
    }
}
