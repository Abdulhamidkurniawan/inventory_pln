<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kantor extends Model
{
    protected $fillable = ['nama', 'singkatan', 'kode']; /* yang bsa di isi */
}
