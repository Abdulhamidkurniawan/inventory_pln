<?php

namespace App\Http\Controllers;

use App\Tipe;
use App\Jenis;
use Illuminate\Http\Request;

class TipeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipes = Tipe::orderBy('jenis', 'ASC')->paginate(10);
        return view('tipe.index', compact('tipes')); /* kirim var */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jeniss = Jenis::All();
        // dd($jeniss);
        return view('tipe.create', compact('jeniss'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'tipe' => 'required',
            'jenis' => 'required|min:3',
            'kode' => 'required' 
        ]);
        Tipe::create([ 
            'tipe' => request('tipe'),
            'jenis' => request('jenis'),
            'kode' => request('kode'),
        ]); 
        return redirect()->route('tipe.index')->withSuccess('Surat berhasil ditambahkan');    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tipe  $tipe
     * @return \Illuminate\Http\Response
     */
    public function show(Tipe $tipe)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tipe  $tipe
     * @return \Illuminate\Http\Response
     */
    public function edit(Tipe $tipe)
    {
        $jeniss = Jenis::All();
        // dd($tipe);
        return view('tipe.edit', compact('tipe', 'jeniss'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tipe  $tipe
     * @return \Illuminate\Http\Response
     */
    public function update(Tipe $tipe)
    {
        $tipe->update([ 
            'tipe' => request('tipe'),
            'jenis' => request('jenis'),
            'kode' => request('kode'),
        ]); 
        return redirect()->route('tipe.index')->withInfo('data berhasil diubah');   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tipe  $tipe
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tipe $tipe)
    {
        $tipe->delete();

        return redirect()->route('tipe.index')->withDanger('data berhasil dihapus');
    }
}
