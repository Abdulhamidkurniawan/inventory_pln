<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Artisan;
class Redirect extends Controller
{
    public function pln()
    {
        return view('auth.login');
    }

    public function cacheclear()
    {
        $exitCode = Artisan::call('cache:clear');
        return '<h1>Cache facade value cleared</h1>';
    }

    public function configcache()
    {
        $exitCode = Artisan::call('config:cache');
        return '<h1>Clear Config cleared</h1>';
    }

    public function routeclear()
    {
        $exitCode = Artisan::call('route:clear');
        return '<h1>Route cache cleared</h1>';
    }

    public function routecache()
    {
        $exitCode = Artisan::call('route:cache');
        return '<h1>Routes cached</h1>';
    }
    public function viewclear()
    {
        $exitCode = Artisan::call('view:clear');
        return '<h1>View cache cleared</h1>';
    }

}
