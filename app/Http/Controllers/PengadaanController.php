<?php

namespace App\Http\Controllers;

use App\Pengadaan;
use Illuminate\Http\Request;

class PengadaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pengadaans = Pengadaan::all();

        return view('pengadaan.index', compact('pengadaans')); /* kirim var */        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pengadaan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'jenis' => 'required',
            'kode' => 'required' 
        ]);
        Pengadaan::create([ 
            'jenis' => request('jenis'),
            'kode' => request('kode'),
        ]); 
        return redirect()->route('pengadaan.index')->withSuccess('Jenis Pengadaan berhasil ditambahkan');    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pengadaan  $pengadaan
     * @return \Illuminate\Http\Response
     */
    public function show(Pengadaan $pengadaan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pengadaan  $pengadaan
     * @return \Illuminate\Http\Response
     */
    public function edit(Pengadaan $pengadaan)
    {
        return view('pengadaan.edit', compact('pengadaan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pengadaan  $pengadaan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pengadaan $pengadaan)
    {
        $pengadaan->update([ 
            'jenis' => request('jenis'),
            'kode' => request('kode'),
        ]); 
        return redirect()->route('pengadaan.index')->withInfo('data berhasil diubah');        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pengadaan  $pengadaan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pengadaan $pengadaan)
    {
        $pengadaan->delete();

        return redirect()->route('pengadaan.index')->withDanger('data berhasil dihapus');    }
}
