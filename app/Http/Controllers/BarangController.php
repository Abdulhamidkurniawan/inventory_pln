<?php

namespace App\Http\Controllers;

use App\Barang;
use App\Jenis;
use App\Tipe;
use App\Kantor;
use App\Pengadaan;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Milon\Barcode\DNS1D;
use App\Exports\BarangExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use PDF;


if(!isset($_SESSION)) {session_start();}
class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $jeniss = Jenis::All();
        $barangs = DB::select("SELECT t1.*
        FROM barangs t1
        WHERE t1.id = (SELECT t2.id
        FROM barangs t2
        WHERE t2.nama = t1.nama
        ORDER BY t2.id DESC
        LIMIT 1)");
        // $barangs = array($barangs);
        $barangs= json_decode( json_encode($barangs), true);
        $s = Barang::all();
        // dd($barangs,$s);

        // $barangs = Barang::all();
        return view('barang.index', compact('barangs','jeniss')); /* kirim var */
    }

    public function search(Request $request)
    {
        $cari= $request->get('nama');
        //  dd($cari);
        $barangs = Barang::where('nama', 'LIKE', '%'.$cari.'%')
                    ->orWhere('kode', 'LIKE', '%'.$cari.'%')
                    ->where('status', 'baik')
                    ->paginate(1000);
        return view('barang.index', compact('barangs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $jeniss = Jenis::All();
        $tipes = Tipe::All();
        $kantors = Kantor::All();
        $pengadaans = Pengadaan::all();
        $jeniss = DB::table('jenis')->pluck("jenis","id");
        // dd($jeniss);
        return view('barang.create', compact('jeniss','tipes','kantors','pengadaans'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $q1 = Barang::select('kode')
                            ->orderBy('created_at', 'DESC')
                            ->take(1)
                            ->get();
        $nomor= json_decode( json_encode($q1), true);
        // $nomor=substr($nomor['kode'],10);
        foreach ($nomor as $key => $value) {
            $nomor = $value;
        }

        if (empty($nomor)) {
            $nomor='0001';
        }
        else {
            $nomor=substr($nomor['kode'],10);
            $nomor=$nomor+1;
        }
        $nomor=sprintf("%'04d", $nomor);
        // dd($nomor);
        $gabung=request('lokasi') .'.'. request('valjenis') .'.'. request('valtipe').'.'. $nomor;
        // dd($gabung,$request->all());
        $this->validate(request(), [
            'nama' => 'required',
            'jenis' => 'required|min:3',
            'tipe' => 'required',
            'lokasi' => 'required',
            'ruang' => 'required',
            'tahun' => 'required',
            'nomor' => 'required',
            'status' => 'required',
            'gambar' => 'required',
            'user' => 'required',
        ]);

        $file = $request->file('gambar');
        $ext = $file->getClientOriginalExtension();
        $newName = rand(100000,1001238912).".".$ext;
        $file->move('uploads/file',$newName);
            // dd($newName,$ext,$file);
        Barang::create([
            'kode' => $gabung,
            'nama' => request('nama'),
            'deskripsi' => request('deskripsi'),
            'jenis' => request('jenis'),
            'tipe' => request('tipe'),
            'lokasi' => request('lokasi'),
            'ruang' => request('ruang'),
            'tahun' => request('tahun'),
            'koordinat' => request('koordinat'),
            'pengadaan' => request('pengadaan'),
            'nomor' => request('nomor'),
            'status' => request('status'),
            'gambar' => $newName,
            'user' => request('user'),
        ]);

        return redirect()->route('barang.index')->withSuccess('Barang berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function show(Barang $barang)
    {
        return view('barang.show', compact('barang'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function edit(Barang $barang)
    {
        $tipes = Tipe::All();
        $kantors = Kantor::All();
        $jeniss = DB::table('jenis')->pluck("jenis","id");
        // dd($jeniss);
        return view('barang.edit', compact('jeniss','tipes','kantors','barang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Barang $barang)
    {

        // $data = $barang->All();
        // $datas = $request->All();
        // dd($request,$request->gambar,$request->file('gambar'));
        if (empty($request->file('gambar'))){
            $update = $request->All();
            $update['gambar'] = $barang->gambar;
            $barang->update($update);

        }
        else{
            unlink('uploads/file/'.$barang->gambar); //menghapus file lama
            $file = $request->file('gambar');
            $ext = $file->getClientOriginalExtension();
            $newName = rand(100000,1001238912).".".$ext;
            $file->move('uploads/file',$newName);
            $update = $request->All();
            $update['gambar'] = $newName;
            $barang->update($update);
    }
    // $update = $request->All();
    // $update['gambar'] = $newName;
    // $barang->update($update);
    return redirect()->route('barang.index')->withInfo('data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function destroy(Barang $barang)
    {
        $kode=$barang->kode;
        // dd($kode,$barang);
        $barangs = Barang::where('kode', $kode)->delete();
        return redirect()->route('barang.index')->withDanger('data berhasil dihapus');
    }

    public function getCountries()
    {
        $jeniss = DB::table('jenis')->pluck("jenis","id");
        dd($jeniss);
        return view('barang.dropdown',compact('jeniss'));
    }

    public function getStates($id)
    {
        $states = DB::table("tipes")->where("jenis",$id)->pluck("tipe","id");
        return json_encode($states);
    }

    public function getValjenis($id)
    {
        $states = DB::table("jenis")->where("jenis",$id)->pluck("kode","jenis");
        return json_encode($states);
    }

    public function getValtipe($id)
    {
        $states = DB::table("tipes")->where("tipe",$id)->pluck("kode","jenis");
        return json_encode($states);
    }

    public function StoreUploadFile(Request $request)
    {
      $validator = Validator::make($request->all(), [
        // 'judul' => 'required',
        'gambar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
      ]);

      if ($validator->passes()) {
        $input = $request->all();
        $input['gambar'] = time().'.'.$request->gambar->getClientOriginalExtension();
        $request->gambar->move(public_path('gambar'), $input['gambar']);

        Upload_file::create($input);
        return response()->json(['success'=>'Berhasil']);
      }

      return response()->json(['error'=>$validator->errors()->all()]);
    }

    public function tambahriwayat(Barang $barang)
    {
        $tipes = Tipe::All();
        $kantors = Kantor::All();
        $jeniss = DB::table('jenis')->pluck("jenis","id");
        // dd($jeniss);
        return view('barang.tambahriwayat', compact('jeniss','tipes','kantors','barang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function riwayatupdate(Request $request, Barang $barang)
    {

        // $data = $barang->All();
        // $datas = $request->All();
        // dd($request,$request->gambar,$request->file('gambar'));
        if (empty($request->file('gambar'))){
            $update = $request->All();
            $update['gambar'] = $barang->gambar;
            Barang::create([
                'kode' => $barang->kode,
                'nama' => $barang->nama,
                'jenis' => $barang->jenis,
                'tipe' => $barang->tipe,
                'lokasi' => $barang->lokasi,
                'ruang' => $barang->ruang,
                'tahun' => $barang->tahun,
                'pengadaan' => $barang->pengadaan,
                'nomor' => $barang->nomor,
                'status' => request('status'),
                'gambar' => $barang->gambar,
                'user' => request('user'),
            ]);
        }
        else{
        $file = $request->file('gambar');
        $ext = $file->getClientOriginalExtension();
        $newName = rand(100000,1001238912).".".$ext;
        $file->move('uploads/file',$newName);
        Barang::create([
            'kode' => $barang->kode,
            'nama' => $barang->nama,
            'jenis' => $barang->jenis,
            'tipe' => $barang->tipe,
            'lokasi' => $barang->lokasi,
            'ruang' => $barang->ruang,
            'tahun' => $barang->tahun,
            'pengadaan' => $barang->pengadaan,
            'nomor' => $barang->nomor,
            'status' => request('status'),
            'gambar' => $newName,
            'user' => request('user'),
        ]);
    }
        return redirect()->route('barang.indexriwayat',$barang)->withInfo('data berhasil diubah');
    }

    public function indexriwayat(Barang $barang)
    {
        // dd($barang->kode);
        $barangs = Barang::where('kode', $barang->kode)->paginate(1000);
        $jeniss = Jenis::All();
        // $alumnis = Alumni::where('nim', 'LIKE', '%'.$cari.'%')->paginate(1000);
        return view('barang.indexriwayat', compact('barangs','jeniss')); /* kirim var */
    }

    public function hapusriwayat(Barang $barang)
    {
        $barang->delete();

        return redirect()->route('barang.index')->withDanger('data berhasil dihapus');
    }

	public function barangexcel()
	{
        // $barangs = Barang::all();
        return (new BarangExport)->download('Barang.xlsx');
        // return view('barang.excel', compact('barangs','jeniss')); /* kirim var */

    }

    public function bcr(Barang $barang)
    {
        $barcode = $barang;
        return view('barang.barcode', compact('barcode'));

        // $bc=DNS1D::getBarcodeSVG($barang->kode, 'c128a');
        // dd($bc);
        // \Storage::disk('public')->put($barcode->kode.'.png',base64_decode(DNS1D::getBarcodePNG($barcode->kode, 'c128a')));
        // return response()->download($barcode->kode.'.png',base64_decode(DNS1D::getBarcodePNG($barcode->kode, 'c128a')));
        $tpl_file = 'barcode/bc.rtf';
        $target = 'barcode/bcx.rtf' ;
        if (file_exists($tpl_file)) {
            $status="File ada";
        // $target = public_path('tmp/ak.rtf') ;
        $f = fopen($tpl_file, "r+");
        $isi = fread($f, filesize($tpl_file));
        fclose($f);
        // dd($barang);
        // dd($target, $barang,$status);

        $isi = str_replace('barcode', $barang->kode, $isi);
        $isi = str_replace('nomor', $barang->kode, $isi);
        $f = fopen($target, "w+");
        fwrite($f, $isi);
        fclose($f);
            $nama_file=$barang->kode.'.doc';
        // header('Location:'.$target);
        header("Content-disposition: attachment; filename=$nama_file");
        header("Content-type: application/octet-stream");
        readfile($target);
        // dd($nama_file);
        // return redirect()->route('barang.index')->withSuccess('data berhasil dicetak');
        return 'data berhasil dicetak';
        }else{echo 'File tidak ada!';
            return redirect()->route('barang.index')->withSuccess('data berhasil dicetak');
        }

    }

    public function barcode(Barang $barang)
    {
        $barcode = $barang;
        // return view('barang.barcode', compact('barcode'));

            // dd("1");
            // $judul="Barcode ".$barcode->kode.".pdf";
        $pdf = PDF::loadView('barang.barcode', compact('barcode'))->setPaper('A4','portrait');
        // dd($judul);
        return $pdf->stream();
        // return $pdf->download($judul);
    }

    public function pdf(Request $request)

    {
        // dd($request->click);
        if ($request->click == "1"){$barangs = Barang::all();
            // dd("1");
            $judul="Semua Barang.pdf";}

        if ($request->click == "2"){
            $dari=$request->dari;
            $sampai=$request->sampai;
            // dd("$request->dari,$request->sampai");
            $barangs = Barang::whereBetween('created_at', [$dari, $sampai])
            // ->orWhere('kode', 'LIKE', '%'.$cari.'%')
            // ->where('status', 'baik')
            ->get();
            // dd("$request->dari,$request->sampai");
            $judul="Laporan Tanggal ".$dari."~".$sampai.".pdf";}

        if ($request->click == "3"){$barangs = Barang::all();
            $status= $request->status;
            $kantor= $request->kantor;

            // dd("3", $cari);
            //  dd($cari);
            $barangs = Barang::where('lokasi', 'LIKE', '%'.$kantor.'%')
                        ->where('status', 'LIKE', '%'.$status.'%')
                        ->get();
            $judul="Laporan Kantor ".$kantor." kondisi ".$status.".pdf";}

        $jeniss = Jenis::All();
        // dd($barangs);
        $pdf = PDF::loadView('barang.pdf', compact('barangs', 'jeniss'))->setPaper('A4','portrait');
        // dd($judul);
        // return $pdf->download($judul);
        return $pdf->stream();

// dd($barangs,$s);

        // $barangs = Barang::all();
        // return view('barang.pdf', compact('barangs','jeniss')); /* kirim var */

        // return $pdf->stream();
    }

    public function report()
    {
        $jeniss = Jenis::All();
        $barangs = Barang::all();
        $kantors = Kantor::All();

// dd($barangs,$s);

        // $barangs = Barang::all();
        return view('barang.report', compact('barangs','jeniss','kantors')); /* kirim var */
    }

    public function dp(Barang $barang)
    {
        return view('barang.dp');
    }

    public function barcodeall(Request $request)
    {
        // $data = $request->id;
        // $data1 = $request->all(); // This will get all the request data.
        $passing = $request['passing'];
        $barangs = Barang::whereIn('id',explode(",",$passing))
                        ->get();
        
        $jumlah = Barang::whereIn('id',explode(",",$passing))
                        ->count();
    
        // dd($passing,$jumlah);
        $barcodes = $barangs;
        $pdf = PDF::loadView('barang.barcodeall', compact('barcodes','jumlah'))->setPaper('A4','portrait');
        return $pdf->stream();


    }
}
