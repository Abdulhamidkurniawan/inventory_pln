<?php

namespace App\Http\Controllers;

use App\Kantor;
use Illuminate\Http\Request;

class KantorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kantors = Kantor::all();

        return view('kantor.index', compact('kantors')); /* kirim var */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kantor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'nama' => 'required',
            'singkatan' => 'required|min:3',
            'kode' => 'required' 
        ]);
        Kantor::create([ 
            'nama' => request('nama'),
            'singkatan' => request('singkatan'),
            'kode' => request('kode'),
        ]); 
        return redirect()->route('kantor.index')->withSuccess('Surat berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Kantor  $kantor
     * @return \Illuminate\Http\Response
     */
    public function show(Kantor $kantor)
    {
        // return view('kantor.show', compact('kantor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Kantor  $kantor
     * @return \Illuminate\Http\Response
     */
    public function edit(Kantor $kantor)
    {
        return view('kantor.edit', compact('kantor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kantor  $kantor
     * @return \Illuminate\Http\Response
     */
    public function update(Kantor $kantor)
    {
        $kantor->update([ 
            'nama' => request('nama'),
            'singkatan' => request('singkatan'),
            'kode' => request('kode'),
        ]); 
        return redirect()->route('kantor.index')->withInfo('data berhasil diubah');   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kantor  $kantor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kantor $kantor)
    {
        $kantor->delete();

        return redirect()->route('kantor.index')->withDanger('data berhasil dihapus');
    }
}
