<?php
 
namespace App\Http\Controllers;
 
use App\Barang;
use App\Jenis;
use App\Tipe;
use App\Kantor;
use Illuminate\Http\Request;
use App\Exports\BarangExport;
use Maatwebsite\Excel\Facades\Excel;
 
class ExportLaravelController extends Controller
{
    function export()
    {
        return Excel::download(new BarangExport, 'Barang.xlsx');
    }

}