<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipe extends Model
{
    protected $fillable = ['tipe', 'jenis', 'kode']; /* yang bsa di isi */
}
