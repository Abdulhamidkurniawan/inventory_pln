<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $fillable = [
        'kode', 
        'nama',
        'deskripsi',
        'jenis', 
        'tipe', 
        'lokasi', 
        'ruang', 
        'koordinat', 
        'tahun', 
        'pengadaan', 
        'nomor', 
        'status', 
        'gambar', 
        'user', 
    ]; /* yang bsa di isi */
}
