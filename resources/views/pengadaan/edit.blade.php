@extends('layouts.app') <!-- --> <!-- include-->

@section('content')
    <div class="container">
        <div class="col-md-8 offset-md-2">
                <div class="card">
                        <div class="card-header">Edit pengadaan</div>
                            <div class="card-body">
                                    <form class="" action="{{ route('pengadaan.update', $pengadaan)}}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('PATCH') }} <!-- membuat patch jenisController bisa dibaca -->
                                            <div class="form-group">
                                            <label for="">Nama pengadaan</label>
                                            <input type="text" class="form-control" name="jenis" placeholder="Nama jenis" value="{{ $pengadaan->jenis }}" required><!-- $mail dari route dan mailController -->
                                            </div>
                                            <div class="form-group">
                                                <label for="">Kode pengadaan</label>
                                            <input type="text" class="form-control" name="kode" placeholder="Kode jenis" value="{{ $pengadaan->kode }}" required><!-- $mail dari route dan mailController -->
                                            </div>
                                                <div class="form-group">
                                                    <input type="submit" class="btn btn-primary" value="Save">
                                                </div>
                                            </form>
                            </div>
                    </div> 
        </div>
    </div>   
@endsection