@extends('layouts.app')

@section('content')
<div class="container">
        <div class="col-md-8 offset-md-2">
            <div class="card">
                @foreach ($pengadaans as $pengadaan)
                <div class="card-header">                  
                    <a href="#" target="_blank"></a>{{$pengadaan->jenis}} ({{$pengadaan->kode}}) - {{$pengadaan->created_at->diffForHumans()}}
                    <div class="float-right">
                            @if (Auth::user()->jabatan == 'admin')
                            <form action="{{ route('pengadaan.destroy', $pengadaan) }}" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }} <!-- membuat delete pengadaanController bisa dibaca -->
                                    <a href="{{ route('pengadaan.edit', $pengadaan) }}" class="btn btn-sm btn-primary">Edit</a>
                                    <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
                            </form>
                            @elseif (Auth::user()->jabatan == 'karyawan')
                            @else {{ route('login') }}
                            @endif
                    </div>
                </div>
            @endforeach
            </div>

        </div>
</div>
@endsection