<!DOCTYPE html>
<html>

<!-- Mirrored from budaya.pln.co.id/auth/login by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 21 Jul 2019 02:05:44 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>

                <meta name="description" content="Aplikasi Komunikasi Manajemen dan Budaya Organisasi PLN">
        <meta name="author" content="PT PLN (Persero)">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    	<meta name="apple-mobile-web-app-capable" content="yes">
    	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	    <meta name="mobile-web-app-capable" content="yes">

        <link rel="manifest" href="assets/js/manifest.json">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="assets/images/logo_pln.jpg">

        <!-- App title -->
        <title>Sibaris</title>

        <!-- App CSS -->
        <link href="assets/css/style.css" rel="stylesheet" type="text/css" />

        <!-- Sweet Alert css -->
        <link href="assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>

        <style>
            .page-login:before {
                position: fixed;
                top: 0;
                left: 0;
                content: '';
                width: 100%;
                height: 100%;
                /*background-image: url("https://budaya.pln.co.id/assets/images/lebaran.png");*/
		        background-image: url("assets/images/bg6.jpg");
                background-position-y: top;
                -webkit-background-size: cover;
                background-size: cover;
                z-index: -1;
            }

            .page-login:after {
                position: fixed;
                top: 0;
                left: 0;
                content: '';
                width: 100%;
                height: 100%;
                /*background-color: rgba(38, 50, 56, 0.85);
                background-color: rgba(72, 80, 88, 0.75);*/
		        background-color: rgba(100, 176, 242, 0.3); /*<--*/
                /*background-color: rgba(100, 176, 242, 0.3);*/
                /*background-color: rgba(27, 185, 154, 0.6);*/
                z-index: -1;
            }
        </style>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117109636-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117109636-1');
</script>


    </head>


    <body class="page-login login">

        <div class=""></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">

        	<div class="account-bg">
                <div class="card-box m-b-0">
                    <div class="text-xs-center m-t-20">
                        <a href="" class="logo">
                            <img src="assets/images/pln.png" height="60" class="img-fluid">
                                                    </a>
                    </div>
                    <div class="m-t-30 m-b-20">
                        <div class="col-xs-12 text-xs-center">
                            <h6 class="text-muted text-uppercase m-b-0 m-t-0">Sign In</h6>
                        </div>
                        <form method="POST" action="{{ route('login') }}">
                        @csrf
                            <div class="form-group ">
                                <div class="col-xs-12">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-xs-12">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror                                </div>
                            </div>

                            <div class="form-group ">
                                <div class="col-xs-12">
                                    <div class="checkbox checkbox-custom">
                                        <input id="checkbox-signup" type="checkbox">
                                        <label for="checkbox-signup">
                                            Remember me
                                        </label>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group text-center m-t-30">
                                <div class="col-xs-12">
                                    <button class="btn btn-success btn-block waves-effect waves-light" type="submit">Log In</button>
                                </div>
                            </div>


                            <div class="form-group m-t-50 m-b-0">
                                <div class="col-sm-12 text-xs-center ">
                                    <div style="color: #8e8e93; font-size: smaller;">
                                        &copy; 2019 <span class=""> PT PLN (Persero)</span>.
                                        <span>All rights reserved</span>
                                    </div>
                                </div>
                            </div>






                        </form>

                    </div>
                </div>
            </div>
            <!-- end card-box-->


        </div>
        <!-- end wrapper page -->


        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/tether.min.js"></script><!-- Tether for Bootstrap -->
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/plugins/switchery/switchery.min.js"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

        <!-- Sweet Alert js -->
        <script src="assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>

        <script src="assets/js/konami.js"></script>
        <script src="assets/js/code.js"></script>
        <script>
            $( window ).load(function() {

            });

        </script>

    <script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","licenseKey":"bb590ce9d8","applicationID":"98050477","transactionName":"MlNRYktSWEFUUBFZCQsZclVNWllcGlIQRA46WlxRUF0=","queueTime":0,"applicationTime":25,"atts":"HhRSFANIS08=","errorBeacon":"bam.nr-data.net","agent":""}</script></body>

<!-- Mirrored from budaya.pln.co.id/auth/login by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 21 Jul 2019 02:05:48 GMT -->
</html>
