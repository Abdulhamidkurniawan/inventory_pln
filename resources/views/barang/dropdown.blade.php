
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

            <div class="form-group">
                <label>Jenis</label>
                <select name="jenis" class="form-control" style="width:250px">
                    <option value="">--- Pilih Jenis ---</option>
                    @foreach ($jeniss as $key => $value)
                    <option value="{{ $value }}">{{ $value }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Tipe</label>
                <select name="tipe" class="form-control"style="width:250px">
                <option>--- Pilih Tipe ---</option>
                </select>
            </div>
      </div>
    <script type="text/javascript">
    jQuery(document).ready(function ()
    {
            jQuery('select[name="jenis"]').on('change',function(){
               var countryID = jQuery(this).val();
               console.log(countryID)
               if(countryID)
               {
                  jQuery.ajax({
                     url : '../../barang/getstates/' +countryID,
                     type : "GET",
                     dataType : "json",
                     cache: false,
                     success:function(data)
                     {
                        alert(countryID);
                        console.log(data);
                        jQuery('select[name="tipe"]').empty();
                        jQuery.each(data, function(key,value){
                           $('select[name="tipe"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });
                     }
                  });
               }
               else
               {
                  $('select[name="tipe"]').empty();
               }
            });
    });
    </script>

