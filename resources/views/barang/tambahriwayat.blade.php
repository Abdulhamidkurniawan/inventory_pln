@extends('layouts.app') <!-- --> <!-- include-->
<?php     ?>
<link rel="stylesheet" href="{{asset('css/app.css')}}">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="http://malsup.github.com/jquery.form.js"></script>

@section('content')

    <div class="container">
        <div class="col-md-8 offset-md-2">
                <div class="card">
                        <div class="card-header">Edit Barang</div>

                        @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="card-body">
                                    <form action="{{ route('barang.riwayatupdate', $barang) }}" enctype="multipart/form-data" method="post">
                                            @csrf
                                            {{ method_field('PATCH') }} <!-- membuat patch barangController bisa dibaca -->
                                            <div class="form-group">
                                                <label for="">Kode Barang</label>
                                                <input type="text" class="form-control" name="kode" placeholder="Status Barang" value="{{$barang->kode}}" disabled><!-- $barang dari route dan barangController -->
                                            </div>    
                                            <div class="form-group">
                                                    <label for="">Status Barang</label>
                                                    <input type="text" class="form-control" name="status" placeholder="Status Barang" value="{{$barang->status}}" required><!-- $barang dari route dan barangController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Gambar</label><br>
                                                    <img src="{{ url('uploads/file/'.$barang->gambar) }}" style="width: 150px; height: 150px;">                            
                                                </div>
                                                <div class="form-group">
                                                    <input type="file" class="form-control" name="gambar" placeholder="Upload Gambar" value="" style="width:250px">
                                                </div>
                                                <div class="form-group">
                                                    {{-- <label for="">User</label> --}}
                                                    <input type="hidden" class="form-control" name="user" placeholder="" value="{{ Auth::user()->name }}" ><!-- $barang dari route dan barangController -->
                                                </div>
                                                <div class="form-group">
                                                    <input type="submit" class="btn btn-primary" value="Save">
                                                </div>
                                            </form>
                            </div>
                    </div> 
        </div>
    </div>
@endsection
