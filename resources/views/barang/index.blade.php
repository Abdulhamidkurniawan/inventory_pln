@extends('layouts.app')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
@section('content')
<div class="container">
  <div class="col-md-10 offset-md-1">
    <div class="card">
                        {!! Form::open (['method'=>'GET','url'=>'caribarang','role'=>'search'])!!}
                            <div class="card-header">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" name="nama" placeholder="Masukan kode atau nama barang" value=""><!-- $borang dari route dan borangController -->
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-sm btn-info">Cari barang</button>
                                    </div>
                                </div>
                            </div>
                        {!! Form::close()!!}
                        <br>
              <form name="barcode" id="barcode" action="{{ route('barang.barcodeall') }}" method="get">	
              {{ csrf_field() }}

              <input type="hidden" id="passing" name="passing" value=""> 

            <table class="table table-hover" id="tblPosts" >
                <thead>
                  <tr>
                    <th scope="col"><center>No</center></th>
                    <th scope="col"><center>Kode</center></th>
                    <th scope="col"><center>Barang</center></th>
                    <th scope="col"><center>Tanggal</center></th>
                    <th scope="col"><center>Riwayat</center></th>
                    <th scope="col"><center>Cetak</center></th>
                    <th scope="col"><center>Pilihan</center></th>
                  </tr>
                </thead>
                <?php $c=0;?>
            @foreach ($barangs as $barang)
            <tbody>
                <tr>
                    <?php $c=$c+1;?>
                <th scope="row"><center>{{$c}}</center></th>
                  <td><center>{{$barang['kode']}}</td>
                  <td><a href="{{ route('barang.show', $barang['id']) }}" >{{$barang['nama']}}</a></td>
                  <td><center>{{substr($barang['created_at'],0,-9)}}</center></td>
                  <td><center>{{$barang['status']}}</center></td>
                  <td><center><form enctype="multipart/form-data" id="my-form" action="{{ route('barang.barcodeall') }}" method="get">
                  <input type="checkbox" name="id[]" value="{{$barang['id']}}" />
                  </form>
                  </td>
                  <td><form action="{{ route('barang.destroy', $barang) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }} <!-- membuat delete barangController bisa dibaca -->
                   <center>
                      <a href="{{ route('barang.edit', $barang['id']) }}" class="btn btn-sm btn-primary">Edit</a>
                      <a href="{{ route('barang.barcode', $barang['id']) }}" class="btn btn-sm btn-info" target="_blank">Barcode</a>
                      <a href="{{ route('barang.indexriwayat', $barang['id']) }}" class="btn btn-sm btn-success">Riwayat</a>
                    @if (Auth::user()->jabatan == 'admin')
                      <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
                    @endif
            </form>
          </center></td>
                </tr>
              </tbody>
              @endforeach
            </table>
            </form>
            {{-- {!! $barangs->render() !!} --}}
        </div>
        <br>
        <a class="btn btn-success" onClick="" href="{{ route('barang.excel') }}">Export Data</a>
        <button class="btn btn-info" type="submit" form="barcode" id="btnSubmit" target="_blank">Cetak Barcode</button>
        {{-- <button type="button" class="btn btn-success" onclick="window.location='{{ route("barang.pdf") }}'">Export Data</button> --}}
</div>

<script language="javascript" type="text/javascript">
$(document).ready(function () {
$("#btnSubmit").click(function(){
var selectedLanguage = new Array();
$('input[name="id[]"]:checked').each(function() {
selectedLanguage.push(this.value);
});
$("#passing").val(selectedLanguage);
alert("Ingin mencetak: "+selectedLanguage.length+" Data");
});
});
</script>
@endsection
