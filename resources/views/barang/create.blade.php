@extends('layouts.app') <!-- --> <!-- include-->
<?php     ?>
<link rel="stylesheet" href="{{asset('css/app.css')}}">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>
<script src="{{ asset('malsup/jquery.form.js') }}"></script>

@section('content')

    <div class="container">
        <div class="col-md-8 offset-md-2">
                <div class="card">
                        <div class="card-header">Tambah Barang</div>

                        @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="card-body">
                                    <form action="{{ route('barang.store') }}" enctype="multipart/form-data" method="post">
                                            @csrf
                                                <div class="form-group">
                                                    <label for="">Nama</label>
                                                    <input type="text" class="form-control" name="nama" id="name" placeholder="Nama Barang" value="{{ old('nama') }}" required><!-- $barang dari route dan barangController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Deskripsi</label>
                                                    <input type="text" class="form-control" name="deskripsi" id="name" placeholder="Deskripsi Barang" value="{{ old('deskripsi') }}" ><!-- $barang dari route dan barangController -->
                                                </div>
                                                    <label>Jenis</label>
                                                    <div class="form-group form-inline">
                                                    <select name="jenis" class="form-control" style="width: 600px">
                                                        <option value="">--- Pilih Jenis ---</option>
                                                        @foreach ($jeniss as $key => $value)
                                                        <option value="{{ $value }}">{{ $value }}</option>
                                                        
                                                        @endforeach
                                                        
                                                    </select>
                                                    <a href="{{ route('jenis.create') }}" class="btn btn-sm btn-primary">Tambah</a>
                                                </div>
                                                <label>Tipe</label>
                                                <div class="form-group form-inline">
                                                <select name="tipe" class="form-control" style="width: 600px">
                                                    <option>--- Pilih Tipe ---</option>
                                                </select>
                                                <a href="{{ route('tipe.create') }}" class="btn btn-sm btn-primary">Tambah</a>
                                                    </div>

                                                    <label for="">Lokasi Unit</label>
                                                <div class="form-group form-inline">
                                                    <select name="lokasi" id="" class="form-control" style="width: 600px">
                                                        @foreach ($kantors as $kantor)  
                                                    <option value="{{$kantor->singkatan}}"> {{$kantor->nama}} </option>
                                                        @endforeach     
                                                    </select>
                                                    <a href="{{ route('kantor.create') }}" class="btn btn-sm btn-primary">Tambah</a>
                                                </div>                                             
                                                <div class="form-group">
                                                    <label for="">Ruang</label>
                                                    <input type="text" class="form-control" name="ruang" placeholder="Ruang" value="{{ old('ruang') }}" required><!-- $barang dari route dan barangController -->
                                                </div>                                                <div class="form-group">
                                                    <label for="">Koordinat</label>
                                                    <input type="text" class="form-control" name="koordinat" placeholder="Koordinat" value="{{ old('koordinat') }}" ><!-- $barang dari route dan barangController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Tahun</label>
                                                <input type="text" class="form-control" name="tahun" placeholder="Tahun Input" value="{{ date("Y")}}" required><!-- $barang dari route dan barangController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Jenis Pengadaan</label>
                                                    <select name="pengadaan" id="" class="form-control">
                                                        @foreach ($pengadaans as $pengadaan)  
                                                    <option value="{{$pengadaan->jenis}}"> {{$pengadaan->jenis}} </option>
                                                        @endforeach     
                                                    </select>                                             </div>
                                                <div class="form-group">
                                                    <label for="">Nomor Kontrak</label>
                                                    <input type="text" class="form-control" name="nomor" placeholder="Nomor Kontrak" value="{{ old('nomor') }}" required><!-- $barang dari route dan barangController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Status Barang</label>
                                                    <select name="status" class="form-control">
                                                        <option value="Baik">Baik</option>
                                                        <option value="Rusak">Rusak</option>
                                                     </select>                                                </div>
                                                <div class="form-group">
                                                    <label for="">Gambar</label>
                                                    <input type="file" class="form-control" name="gambar" placeholder="Upload Gambar" value="{{ old('gambar') }}" style="width:250px">
                                                </div>
                                                <div class="form-group">
                                                    {{-- <label for="">User</label> --}}
                                                    <input type="hidden" class="form-control" name="user" placeholder="" value="{{ Auth::user()->name }}" ><!-- $barang dari route dan barangController -->
                                                </div>
                                                <div class="valtipe">
                                                    {{-- <label for="">Coba tipe</label> --}}
                                                    <input type="hidden"  class="form-control" name="valtipe" value="" required><!-- $barang dari route dan barangController -->
                                                </div>  
                                                <div class="valjenis">
                                                    {{-- <label for="">Coba jeniss</label> --}}
                                                    <input type="hidden"  class="form-control" name="valjenis" value="" required><!-- $barang dari route dan barangController -->
                                                </div>  
                                                <div class="form-group">
                                                    <button  type="submit" class="btn btn-primary" value="Submit">Simpan</button>
                                                </div>
                                            </form>
                            </div>
                    </div> 
        </div>
    </div>
    <script type="text/javascript">
        jQuery(document).ready(function ()
        {
                jQuery('select[name="jenis"]').on('change',function(){
                   var countryID = jQuery(this).val();
                   console.log(countryID)   
                //    alert(countryID);
                   if(countryID)
                   {
                      jQuery.ajax({
                         url : 'http://'+document.location.hostname+'/inventory_pln/public/barang/getstates/' +countryID,
                         type : "GET",
                         dataType : "json",
                         cache: false,
                         success:function(data)
                         {
                            // alert(countryID);
                            console.log(data);
                            jQuery('select[name="tipe"]').empty();
                            jQuery.each(data, function(key,value){
                               $('select[name="tipe"]').append('<option value="'+ value +'">'+ value +'</option>');
                            });
                         }
                      });

                    jQuery.ajax({
                    url : 'http://'+document.location.hostname+'/inventory_pln/public/barang/getvaljenis/' +countryID,
                    type : "GET",
                    dataType : "json",
                    cache: false,
                    success:function(data){
                        // alert(countryID);
                        console.log(data);
                        jQuery('.valjenis input').empty();
                        jQuery.each(data, function(key,value){
                            $('.valjenis input').val(value);
                        });
                    }
                  });
                   }
                   else
                   {
                      $('select[name="tipe"]').empty();
                   }
                });
        });
        </script>   
        
        <script type="text/javascript">
            jQuery(document).ready(function ()
            {
                    jQuery('select[name="tipe"]').on('click change',function(){
                       var countryID = jQuery(this).val();
                    //    alert(countryID);
                       console.log(countryID)
                        jQuery.ajax({
                    url : 'http://'+document.location.hostname+'/inventory_pln/public/barang/getvaltipe/' +countryID,
                    type : "GET",
                    dataType : "json",
                    cache: false,
                    success:function(data){
                        // alert(countryID);
                        console.log(data);
                        jQuery('.valtipe input').empty();
                        jQuery.each(data, function(key,value){
                            $('.valtipe input').val(value);
                        });
                    }
                  });
                    });
            });
            </script>   
@endsection
