            <table class="table">
                <thead>
                  <tr>
                    <th scope="col">No</th>
                    <th scope="col">Kode</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Deskripsi</th>
                    <th scope="col">Jenis</th>
                    <th scope="col">Tipe</th>
                    <th scope="col">Lokasi</th>
                    <th scope="col">Ruang</th>
                    <th scope="col">Koordinat</th>
                    <th scope="col">Tahun</th>
                    <th scope="col">Jenis Pengadaan</th>
                    <th scope="col">Status</th>
                  </tr>
                </thead>
                <?php $c=0;?>
            @foreach ($barangs as $barang)
            <tbody>
                <tr>
                    <?php $c=$c+1;?>
                <td scope="row" >{{$c}}</td>
                    <td>{{$barang['kode']}}</td>
                    <td>{{$barang['nama']}}</td>
                    <td>{{$barang['deksripsi']}}</td>
                    <td>{{$barang['jenis']}}</td>
                    <td>{{$barang['tipe']}}</td>
                    <td>{{$barang['lokasi']}}</td>
                    <td>{{$barang['ruang']}}</td>
                    <td>{{$barang['koordinat']}}</td>
                    <td>{{substr($barang['created_at'],0,-9)}}</td>
                    <td>{{$barang['pengadaan']}}</td>
                    <td>{{$barang['status']}}</td>

                </tr>
              </tbody>
              @endforeach
            </table>
