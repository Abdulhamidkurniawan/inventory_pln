<script src="{{ asset('js/app.js') }}" defer></script>
<link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}">
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> --}}

<!-- Fonts -->
<link rel="dns-prefetch" href="//fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('css/app.css', array('media' => 'print'))}}">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="{{ asset('malsup/jquery.form.js') }}"></script>
<style>
.container {
  position: relative;
  text-align: left;
  top: 0px;
  left: -35px;
}

.top-left {
  color: black;
  position: absolute;
  top: 62px;
  left: 110px;
}

.kode {
  color: black;
  position: absolute;
  top: 68px;
  left: 170px;
}

        .myTableBg {
            width:100%  ;
            text-align: left;
            background: white url('{{asset('images/barcode.png')}}') ;
            -webkit-print-color-adjust: exact;
            background-size: 50%;
            border-collapse: collapse;
            background-repeat: no-repeat;
            /* background-position: center center; */
            border: 0px solid black;
            }
        </style>
<style type="text/css">
	img{
		padding-left: 20px;
	}
</style>
<style>
    body {
    background-color: #ffffff
    }
    </style>
    <!-- <script type="text/javascript">setTimeout("window.close();", 1000);</script> -->

    <body>
    <!-- <body onload="window.print();window.close()"> -->

{{-- <div class="container text-center" style="border: 1px solid #a1a1a1;padding: 15px;width: 70%;"> --}}
<table style="width:47%" border="1" >
<?php 
$c=0;
for ($i=0; $i<$jumlah; $i++){
?>
<tr>
<?php 
  if(($jumlah-$c)==1){
    for ($j=0; $j<1; $j++){
      ?>   
      <td height="90">  
        <div class="container">
        <img src="{{ url('images/barcode.png') }}" style="width: 340px; height: 120px;">  
        <div class="top-left">         
        <img src="data:image/png;base64,{{DNS1D::getBarcodePNG($barcodes[$c]->kode, 'c128a')}}" height="30 " width="235" alt="barcode" onclick="javascript:window.print();"/></th>
        </div>    
        <div class="kode">         
        <font size="16" color="black">{{$barcodes[$c]->kode}}</font>
        </div>
        </div>
      </td>
      <?php 
    }
  }else{
  
  for ($j=0; $j<2; $j++){
  ?>   
  <td height="90">  
    <div class="container">
    <img src="{{ url('images/barcode.png') }}" style="width: 340px; height: 120px;">  
    <div class="top-left">         
    <img src="data:image/png;base64,{{DNS1D::getBarcodePNG($barcodes[$c]->kode, 'c128a')}}" height="30 " width="235" alt="barcode" onclick="javascript:window.print();"/></th>
    </div>    
    <div class="kode">         
    <font size="16" color="black">{{$barcodes[$c]->kode}}</font>
    </div>
    </div>
  </td>
  <?php 
    $c=$c+1;
  }} ?>
 </tr>
<?php 
$i = $i+1;
} ?>
  </table> 
  
<!-- <table style="width:47%" border="1" >
@foreach ($barcodes as $barcode)
  <tr>            
  <td height="90">  
    <div class="container">
    <img src="{{ url('images/barcode.png') }}" style="width: 340px; height: 120px;">  
    <div class="top-left">         
    <img src="data:image/png;base64,{{DNS1D::getBarcodePNG($barcode->kode, 'c128a')}}" height="30 " width="235" alt="barcode" onclick="javascript:window.print();"/></th>
    </div>    
    <div class="kode">         
    <font size="16" color="black">{{$barcode->kode}}</font>
    </div>
    </div>
  </td>
  </tr>
@endforeach
</table>  -->

	{{-- <a href="data:image/png;base64,{{DNS1D::getBarcodePNG($barcode->kode, 'c128a')}}">Download</a>
	<button download="{{$barcode->kode}}.png" onclick="javascript:window.print();">Download</button> --}}

{{--
	<a download="{{$barcode->kode}}.png" onclick="javascript:alert('My First JavaScript');" href="data:image/png;base64,{{DNS1D::getBarcodePNG($barcode->kode, 'c128a') }}">Download</a>
	<img src="data:image/png;base64,{{DNS1D::getBarcodePNG('MHK.01.01.0999', 'C39+')}}" alt="barcode" /><br><br>
	<img src="data:image/png;base64,{{DNS1D::getBarcodePNG('MHK.01.01.0999', 'C39E')}}" alt="barcode" /><br><br>
	<img src="data:image/png;base64,{{DNS1D::getBarcodePNG('MHK.01.01.0999', 'C39E+')}}" alt="barcode" /><br><br>
	<img src="data:image/png;base64,{{DNS1D::getBarcodePNG('MHK.01.01.0999', 'C93')}}" alt="barcode" /><br><br>
	<br/>
	<br/>
	<img src="data:image/png;base64,{{DNS1D::getBarcodePNG('19', 'S25')}}" alt="barcode" /><br><br>
	<img src="data:image/png;base64,{{DNS1D::getBarcodePNG('20', 'S25+')}}" alt="barcode" /><br><br>
	<img src="data:image/png;base64,{{DNS1D::getBarcodePNG('21', 'I25')}}" alt="barcode" /><br><br>
	<img src="data:image/png;base64,{{DNS1D::getBarcodePNG('22', 'MSI+')}}" alt="barcode" /><br><br>
	<img src="data:image/png;base64,{{DNS1D::getBarcodePNG('23', 'POSTNET')}}" alt="barcode" /><br><br>
	<br/>
	<br/>
	<img src="data:image/png;base64,{{DNS2D::getBarcodePNG('MHK.01.01.0999', 'QRCODE')}}" alt="barcode" /><br><br>
	<img src="data:image/png;base64,{{DNS2D::getBarcodePNG('MHK.01.01.0999', 'PDF417')}}" alt="barcode" /><br><br>
	<img src="data:image/png;base64,{{DNS2D::getBarcodePNG('MHK.01.01.0999', 'DATAMATRIX')}}" alt="barcode" /><br><br> --}}
{{-- </div> --}}
</body>
