@extends('layouts.app')
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
$.noConflict();
jQuery( document ).ready(function( $ ) {
  $( function() {
  $( "#dari" ).datepicker({dateFormat: 'yy-mm-dd'});
} );
$( function() {
  $( "#sampai" ).datepicker({dateFormat: 'yy-mm-dd'});
} );
  // Code that uses jQuery's $ can follow here.
});
// Code that uses other library's $ can follow here.
</script>

@section('content')
<div class="container">
  <div class="col-md-8 offset-md-2">
    <div class="card">
                        {{-- {!! Form::open (['method'=>'GET','url'=>'caribarang','role'=>'search'])!!}
                            <div class="card-header">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" name="nama" placeholder="Masukan kode atau nama barang" value=""><!--  dari route dan borangController -->
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-sm btn-info">Cari barang</button>
                                    </div>
                                </div>
                            </div>
                        {!! Form::close()!!}
                        <br> --}}
            <table class="table table-hover" >
                <thead>
                  <tr>
                    <th class="w-25" scope="col"><center>Export</center></th>
                    <th class="w-25" scope="col"><center>Tanggal</center></th>
                    <th class="w-25" scope="col"><center>Kondisi</center></th>
                  </tr>
                </thead>
                <?php $c=0;?>
            <tbody>

                <tr>
                      <td><center>Export<br>
                        Semua Data
                          <br>
                          <br>
                          {!! Form::open (['method'=>'GET','url'=>'pdf','role'=>'search'])!!}
                          <button type="submit" class="btn btn-sm btn-success" name="click" value="1">Cetak</button>
                          {!! Form::close()!!}
                      </center></td>
                      <td><center>
                          {!! Form::open (['method'=>'GET','url'=>'pdf','role'=>'search'])!!}
                          <input type="text" id="dari" name="dari" size="10" placeholder="Dari"> <br>
                          <input type="text" id="sampai" name="sampai" size="10" placeholder="Sampai">
                          <br>
                          <br>
                          <button type="submit" class="btn btn-sm btn-success" name="click" value="2">Cetak</button>
                          {!! Form::close()!!}
                        </center></td>
                        <td><center>
                            <form method="GET" action="{{route('barang.pdf')}}" role="search">
                              <select name="kantor" id="" class="form-control">
                                <option value="">--- Pilih Kantor ---</option>
                                @foreach ($kantors as $kantor)
                            <option value="{{$kantor->singkatan}}"> {{$kantor->nama}} </option>
                                @endforeach
                            </select>
                              <select name="status" class="form-control" >
                                <option value="">--- Pilih Kondisi ---</option>
                                <option value="Baik">Baik</option>
                                <option value="Rusak">Rusak</option>
                              </select>
                              <br>
                                  <button type="submit" class="btn btn-sm btn-success" name="click" value="3">Cetak</button>
                              </form>
                        </center></td>
                    </tr>
{{-- <td><center>{{substr($barang['created_at'],0,-9)}}</center></td> --}}
              </tbody>
            </table>

            <br>
            <br>
            {{-- {!! $barangs->render() !!} --}}
        </div>
        <br>
        {{-- <a class="btn btn-success" href="{{ route('barang.export') }}">Export Data</a> --}}
        {{-- <button type="button" class="btn btn-success" onclick="window.location='{{ route("barang.pdf") }}'">Export Data</button> --}}
</div>
@endsection
