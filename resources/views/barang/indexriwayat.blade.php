@extends('layouts.app')

@section('content')
<div class="container">
        <div class="col-md-9 offset-md-1">
                <div class="card">
                        {{-- {!! Form::open (['method'=>'GET','url'=>'caribarang','role'=>'search'])!!}
                            <div class="card-header">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" name="nama" placeholder="Masukan nama" value=""><!-- $borang dari route dan borangController -->
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-sm btn-info">Cari barang</button>
                                    </div>
                                </div>
                            </div>
                        {!! Form::close()!!} --}}
                        <br>
            <table class="table table-hover" >
                <thead>
                  <tr>
                    <th scope="col"><center>No</center></th>
                    <th scope="col"><center>Kode</center></th>
                    <th scope="col"><center>Barang</center></th>
                    <th scope="col"><center>Tanggal</center></th>
                    <th scope="col"><center>Riwayat</center></th>
                    @if (Auth::user()->jabatan == 'admin')
                    <th scope="col"><center>User</center></th>
                    @endif
                    <th scope="col"><center>Pilihan</center></th>
                  </tr>
                </thead>
                <?php $c=0;?>
            @foreach ($barangs as $barang)
            <tbody>
                <tr>
                    <?php $c=$c+1;?>
                <th scope="row"><center>{{$c}}</center></th>
                  <td><center>{{$barang->kode}}</td>
                  <td><a href="{{ route('barang.show', $barang) }}" >{{$barang->nama}}</a></td>
                  <td><center>{{substr($barang->created_at,0,-9)}}</center></td>
                  <td><center>{{$barang->status}}</center></td>
                  @if (Auth::user()->jabatan == 'admin')
                  <td><center>{{$barang->user}}</center></td>
                  @endif
                  <td><center><form action="{{ route('barang.hapusriwayat', $barang) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }} <!-- membuat delete barangController bisa dibaca -->
                    <a href="{{ route('barang.tambahriwayat', $barang) }}" class="btn btn-sm btn-success">Tambah Riwayat</a>
                    @if (Auth::user()->jabatan == 'admin')
                    <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
                    @endif
            </form></center></td>
                </tr>
              </tbody>
              @endforeach

            </table>
            <br>
            <br>
            {{-- {!! $barangs->render() !!} --}}
        </div>
</div>
@endsection
