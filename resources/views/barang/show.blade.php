@extends('layouts.app') <!-- --> <!-- include-->
<script src="{{ asset('malsup/jquery.form.js') }}"></script>

@section('content')
    <div class="container">
        <div class="row">
        <div class="col-md-8 offset-md-2">
                <div class="card">
                        <div class="card-header">{{$barang->kode}} | <small>{{$barang->nama}}</small></div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="">Nama</label>
                                <input type="text" class="form-control" name="nama" placeholder="NIM Mahasiswa" value="{{$barang->nama}}" disabled><!-- $barang dari route dan barangController -->
                            </div>
                            <div class="form-group">
                                <label for="">Deskripsi</label>
                                <input type="text" class="form-control" name="deskripsi" placeholder="NIM Mahasiswa" value="{{$barang->deskripsi}}" disabled><!-- $barang dari route dan barangController -->
                                </div>
                            <div class="form-group">
                                <label for="">Jenis</label>
                                <input type="text" class="form-control" name="jenis" placeholder="Nama Mahasiswa" value="{{$barang->jenis}}" disabled><!-- $barang dari route dan barangController -->
                            </div>
                            <div class="form-group">
                                <label for="">Tipe</label>
                                <input type="text" class="form-control" name="tipe" placeholder="Nama Mahasiswa" value="{{$barang->tipe}}" disabled><!-- $barang dari route dan barangController -->
                            </div>
                            <div class="form-group">
                                <label for="">Lokasi Unit</label>
                                <input type="text" class="form-control" name="lokasi" placeholder="Telp Mahasiswa" value="{{$barang->lokasi}}" disabled><!-- $barang dari route dan barangController -->
                            </div>
                            <div class="form-group">
                                <label for="">Ruang</label>
                                <input type="text" class="form-control" name="ruang" placeholder="D-III/D-IV" value="{{$barang->ruang}}" disabled><!-- $barang dari route dan barangController -->
                            </div>
                            </div>                                                
                            <div class="form-group">
                                <label for="">Koordinat</label>
                                <input type="text" class="form-control" name="koordinat" placeholder="Koordinat" value="{{$barang->koordinat}}" disabled><!-- $barang dari route dan barangController -->
                            </div>
                            <div class="form-group">
                                <label for="">Tahun</label>
                            <input type="text" class="form-control" name="tahun" placeholder="Tahun Input" value="{{$barang->tahun}}" disabled><!-- $barang dari route dan barangController -->
                            </div>
                            <div class="form-group">
                                <label for="">Jenis Pengadaan</label>
                                <input type="text" class="form-control" name="pengadaan" placeholder="Jenis Pengadaan" value="{{$barang->pengadaan}}" disabled><!-- $barang dari route dan barangController -->
                            </div>
                            <div class="form-group">
                                <label for="">Nomor</label>
                                <input type="text" class="form-control" name="nomor" placeholder="Nomor" value="{{$barang->nomor}}" disabled><!-- $barang dari route dan barangController -->
                            </div>
                            <div class="form-group">
                                <label for="">Status Barang</label>
                                <input type="text" class="form-control" name="status" placeholder="Status Barang" value="{{$barang->status}}" disabled><!-- $barang dari route dan barangController -->
                            </div>
                            <div class="form-group">
                                <label for="">Gambar</label>
                                <br>
                                <img src="{{ url('uploads/file/'.$barang->gambar) }}" style="width: 150px; height: 150px;">                            
                            </div>
                            <button type="button" class="btn btn-primary" onclick="window.location='{{ URL::previous() }}'">Kembali</button>
                            {{-- <button type="button" class="btn btn-success" onclick="window.location='{{ route("barang.riwayat") }}'">Tambah Riwayat Barang</button> --}}
                </div>
                </div>
            <br>
        </div>      
        </div>
    </div>   
@endsection