<img src="{{asset('images/kop.png')}}" height="140" >

@extends('layouts.rpt')
<!-- <img src="{{asset('images/kop.png')}}" height="40" > -->
<style>
td {
  height: 10px;
  vertical-align: middle;
}

body {
  background-color: white;
}
</style>

@section('content')
<!-- <img src="{{asset('images/kop.png')}}" height="40" > -->
<div class="container">
  <div class="">
    <div class="card">

            <table class="table">
                <thead>
                  <tr>
                    <th scope="col" style="width:20px"><center>No</center></th>
                    <th scope="col"><center>Kode</center></th>
                    <th scope="col"><center>Nama</center></th>
                    <th scope="col"><center>Jenis</center></th>
                    <th scope="col"style="width:40px"><center>Tipe</center></th>
                    <th scope="col"style="width:40px"><center>Ruang</center></th>
                    <th scope="col"><center>Tanggal</center></th>
                    <th scope="col"><center>Metode Pembelian</center></th>
                    <th scope="col"style="width:40px"><center>Status</center></th>
                  </tr>
                </thead>
                <?php $c=0;?>
            @foreach ($barangs as $barang)
            <tbody>
                <tr>
                    <?php $c=$c+1;?>
                <td scope="row" ><center>{{$c}}</center></td>
                  <td><center>{{$barang['kode']}}</td>
                  <td><center>{{$barang['nama']}}</a></td>
                  <td><center>{{$barang['jenis']}}</center></td>
                  <td><center>{{$barang['tipe']}}</center></td>
                  <td><center>{{$barang['ruang']}}</center></td>
                  <td><center>{{substr($barang['created_at'],0,-9)}}</center></td>
                  <td><center>{{$barang['pengadaan']}}</center></td>
                  <td><center>{{$barang['status']}}</center></td>

                </tr>
              </tbody>
              @endforeach
            </table>

            <br>
            <br>
            {{-- {!! $barangs->render() !!} --}}
        </div>
        <br>
        {{-- <a class="btn btn-success" href="{{ route('barang.export') }}">Export Data</a> --}}
</div>
@endsection
