@extends('layouts.app')

@section('content')
<div class="container">
        <div class="col-md-8 offset-md-2">
            <div class="card">
                @foreach ($jeniss as $jenis)
                <div class="card-header">                  
                    <a href="#" target="_blank"></a>{{$jenis->jenis}} ({{$jenis->kode}}) - {{$jenis->created_at->diffForHumans()}}
                    <div class="float-right">
                            @if (Auth::user()->jabatan == 'admin')
                            <form action="{{ route('jenis.destroy', $jenis) }}" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }} <!-- membuat delete jenisController bisa dibaca -->
                                    <a href="{{ route('jenis.edit', $jenis) }}" class="btn btn-sm btn-primary">Edit</a>
                                    <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
                            </form>
                            @elseif (Auth::user()->jabatan == 'karyawan')
                            @else {{ route('login') }}
                            @endif
                    </div>
                </div>
            @endforeach
            </div>

        </div>
</div>
@endsection