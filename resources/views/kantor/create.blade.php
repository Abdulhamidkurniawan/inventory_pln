@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <div class="card-body">
                    <form method="POST" action="{{ route('kantor.store') }}">
                        @csrf
                        {{-- {{ route('register') }} --}}
                        <div class="form-group">
                                                    <label for="">Nama Kantor</label>
                                                <input type="text" class="form-control" name="nama" placeholder="Nama Kantor" value="{{ old('nama') }}" required><!-- $mail dari route dan mailController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Singkatan</label>
                                                <input type="text" class="form-control" name="singkatan" placeholder="Singkatan" value="{{ old('singkatan') }}" required><!-- $mail dari route dan mailController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Kode Kantor</label>
                                                <input type="text" class="form-control" name="kode" placeholder="Kode Kantor" value="{{ old('kode') }}" required><!-- $mail dari route dan mailController -->
                                                </div>
                                                <div class="form-group">
                                                    <input type="submit" class="btn btn-primary" value="Save">
                                                </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
