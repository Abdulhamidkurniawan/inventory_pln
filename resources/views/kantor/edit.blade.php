@extends('layouts.app') <!-- --> <!-- include-->

@section('content')
    <div class="container">
        <div class="col-md-8 offset-md-2">
                <div class="card">
                        <div class="card-header">Edit Kantor</div>
                            <div class="card-body">
                                    <form class="" action="{{ route('kantor.update', $kantor)}}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('PATCH') }} <!-- membuat patch kantorController bisa dibaca -->
                                            <div class="form-group">
                                            <label for="">Nama Kantor</label>
                                            <input type="text" class="form-control" name="nama" placeholder="Nama Kantor" value="{{ $kantor->nama }}" required><!-- $mail dari route dan mailController -->
                                            </div>
                                            <div class="form-group">
                                                <label for="">Singkatan</label>
                                            <input type="text" class="form-control" name="singkatan" placeholder="Singkatan" value="{{ $kantor->singkatan }}" required><!-- $mail dari route dan mailController -->
                                            </div>
                                            <div class="form-group">
                                                <label for="">Kode Kantor</label>
                                            <input type="text" class="form-control" name="kode" placeholder="Kode Kantor" value="{{ $kantor->kode }}" required><!-- $mail dari route dan mailController -->
                                            </div>
                                                <div class="form-group">
                                                    <input type="submit" class="btn btn-primary" value="Save">
                                                </div>
                                            </form>
                            </div>
                    </div> 
        </div>
    </div>   
@endsection