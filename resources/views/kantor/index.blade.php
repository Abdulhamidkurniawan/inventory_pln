@extends('layouts.app')

@section('content')
<div class="container">
        <div class="col-md-8 offset-md-2">
            <div class="card">
                @foreach ($kantors as $kantor)
                <div class="card-header">                  
                    <a href="#" target="_blank"></a>{{$kantor->nama}} ({{$kantor->singkatan}}) - {{$kantor->created_at->diffForHumans()}}
                    <div class="float-right">
                            @if (Auth::user()->jabatan == 'admin')
                            <form action="{{ route('kantor.destroy', $kantor) }}" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }} <!-- membuat delete kantorController bisa dibaca -->
                                    <a href="{{ route('kantor.edit', $kantor) }}" class="btn btn-sm btn-primary">Edit</a>
                                    <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
                            </form>
                            @elseif (Auth::user()->jabatan == 'karyawan')
                            @else {{ route('login') }}
                            @endif
                    </div>
                </div>
            @endforeach
            </div>

        </div>
</div>
@endsection