@extends('layouts.app')

@section('content')
<div class="container">
        <div class="col-md-8 offset-md-2">
            <div class="card">
                @foreach ($tipes as $tipe)
                <div class="card-header">                  
                    <a href="#" target="_blank"></a>{{$tipe->tipe}} ({{$tipe->jenis}},{{$tipe->kode}}) - {{$tipe->created_at->diffForHumans()}}
                    <div class="float-right">
                            @if (Auth::user()->jabatan == 'admin')
                            <form action="{{ route('tipe.destroy', $tipe) }}" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }} <!-- membuat delete tipeController bisa dibaca -->
                                    <a href="{{ route('tipe.edit', $tipe) }}" class="btn btn-sm btn-primary">Edit</a>
                                    <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
                            </form>
                            @elseif (Auth::user()->jabatan == 'karyawan')
                            @else {{ route('login') }}
                            @endif
                    </div>
                </div>
            @endforeach

            </div>

        </div>
        <div class="col-md-8 offset-md-5">
            <br>
            {!! $tipes->render() !!}
        </div>
</div>
@endsection