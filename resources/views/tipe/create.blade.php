@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <div class="card-body">
                    <form method="POST" action="{{ route('tipe.store') }}">
                        @csrf
                        {{-- {{ route('register') }} --}}
                        <div class="form-group">
                                                    <label for="">Nama</label>
                                                <input type="text" class="form-control" name="tipe" placeholder="Nama " value="{{ old('tipe') }}" required><!-- $mail dari route dan mailController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Jenis</label>
                                                    <select name="jenis" id="" class="form-control">
                                                            @foreach ($jeniss as $jenis)  
                                                    <option value="{{$jenis->jenis}}"> {{$jenis->jenis}} </option>
                                                            @endforeach     
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Kode </label>
                                                <input type="text" class="form-control" name="kode" placeholder="Kode " value="{{ old('kode') }}" required><!-- $mail dari route dan mailController -->
                                                </div>
                                                <div class="form-group">
                                                    <input type="submit" class="btn btn-primary" value="Save">
                                                </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
