@extends('layouts.app') <!-- --> <!-- include-->

@section('content')
    <div class="container">
        <div class="col-md-8 offset-md-2">
                <div class="card">
                        <div class="card-header">Edit Tipe</div>
                            <div class="card-body">
                                    <form class="" action="{{ route('tipe.update', $tipe)}}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('PATCH') }} <!-- membuat patch tipeController bisa dibaca -->
                                            <div class="form-group">
                                                <label for="">Nama tipe</label>
                                            <input type="text" class="form-control" name="tipe" placeholder="Nama tipe" value="{{ $tipe->tipe }}" required><!-- $mail dari route dan mailController -->
                                            </div>
                                            <div class="form-group">
                                                    <label for="">Jenis</label>
                                                    <select name="jenis" id="" class="form-control">
                                                            @foreach ($jeniss as $jenis)   <!-- $categories dari PostController -->
                                                    <option value="{{$jenis->jenis}}"
                                                    @if ($jenis->jenis === $tipe->jenis)  
                                                        selected
                                                    @endif
                                                    > {{$jenis->jenis}} </option>
                                                            @endforeach     
                                                    </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="">Kode tipe</label>
                                            <input type="text" class="form-control" name="kode" placeholder="Kode tipe" value="{{ $tipe->kode }}" required><!-- $mail dari route dan mailController -->
                                            </div>
                                                <div class="form-group">
                                                    <input type="submit" class="btn btn-primary" value="Save">
                                                </div>
                                            </form>
                            </div>
                    </div> 
        </div>
    </div>   
@endsection