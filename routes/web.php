<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Redirect@pln')->name('pln');
Route::get('/cacheclear', 'Redirect@cacheclear')->name('cacheclear');
Route::get('/configcache', 'Redirect@configcache')->name('configcache');
Route::get('/routeclear', 'Redirect@routeclear')->name('routeclear');
Route::get('/routecache', 'Redirect@routecache')->name('routecache');
Route::get('/viewclear', 'Redirect@viewclear')->name('viewclear');


Route::middleware('auth')->group(function() {

Route::get('/kantor', 'KantorController@index')->name('kantor.index');
Route::get('/kantor/create', 'KantorController@create')->name('kantor.create');
Route::post('/kantor/create', 'KantorController@store')->name('kantor.store');
Route::get('/kantor/{kantor}', 'KantorController@show')->name('kantor.show');
Route::get('/kantor/{kantor}/edit', 'KantorController@edit')->name('kantor.edit');
Route::patch('/kantor/{kantor}/edit', 'KantorController@update')->name('kantor.update');
Route::delete('/kantor/{kantor}/delete', 'KantorController@destroy')->name('kantor.destroy');

Route::get('/jenis', 'JenisController@index')->name('jenis.index');
Route::get('/jenis/create', 'JenisController@create')->name('jenis.create');
Route::post('/jenis/create', 'JenisController@store')->name('jenis.store');
Route::get('/jenis/{jenis}', 'JenisController@show')->name('jenis.show');
Route::get('/jenis/{jenis}/edit', 'JenisController@edit')->name('jenis.edit');
Route::patch('/jenis/{jenis}/edit', 'JenisController@update')->name('jenis.update');
Route::delete('/jenis/{jenis}/delete', 'JenisController@destroy')->name('jenis.destroy');

Route::get('/tipe', 'TipeController@index')->name('tipe.index');
Route::get('/tipe/create', 'TipeController@create')->name('tipe.create');
Route::post('/tipe/create', 'TipeController@store')->name('tipe.store');
Route::get('/tipe/{tipe}', 'TipeController@show')->name('tipe.show');
Route::get('/tipe/{tipe}/edit', 'TipeController@edit')->name('tipe.edit');
Route::patch('/tipe/{tipe}/edit', 'TipeController@update')->name('tipe.update');
Route::delete('/tipe/{tipe}/delete', 'TipeController@destroy')->name('tipe.destroy');

Route::get('/pengadaan', 'PengadaanController@index')->name('pengadaan.index');
Route::get('/pengadaan/create', 'PengadaanController@create')->name('pengadaan.create');
Route::post('/pengadaan/create', 'PengadaanController@store')->name('pengadaan.store');
Route::get('/pengadaan/{pengadaan}', 'PengadaanController@show')->name('pengadaan.show');
Route::get('/pengadaan/{pengadaan}/edit', 'PengadaanController@edit')->name('pengadaan.edit');
Route::patch('/pengadaan/{pengadaan}/edit', 'PengadaanController@update')->name('pengadaan.update');
Route::delete('/pengadaan/{pengadaan}/delete', 'PengadaanController@destroy')->name('pengadaan.destroy');

Route::get('/barang', 'BarangController@index')->name('barang.index');
Route::get('/barang/create', 'BarangController@create')->name('barang.create');
Route::post('/barang/create', 'BarangController@store')->name('barang.store');
Route::get('/barang/{barang}', 'BarangController@show')->name('barang.show');
Route::get('/barang/{barang}/edit', 'BarangController@edit')->name('barang.edit');
Route::patch('/barang/{barang}/edit', 'BarangController@update')->name('barang.update');
Route::delete('/barang/{barang}/delete', 'BarangController@destroy')->name('barang.destroy');
Route::get('/caribarang', 'BarangController@search')->name('barang.cari');

Route::get('/barangs','BarangController@getCountries');
Route::get('/barang/getstates/{id}','BarangController@getStates')->name('barang.getstates');
Route::get('/barang/getvaljenis/{id}','BarangController@getValjenis');
Route::get('/barang/getvaltipe/{id}','BarangController@getValtipe');
Route::get('/barang/{barang}/riwayat', 'BarangController@indexriwayat')->name('barang.indexriwayat');
Route::get('/barang/{barang}/tambahriwayat', 'BarangController@tambahriwayat')->name('barang.tambahriwayat');
Route::patch('/barang/{barang}/tambahriwayat', 'BarangController@riwayatupdate')->name('barang.riwayatupdate');
Route::delete('/barang/{barang}/hapusriwayat', 'BarangController@hapusriwayat')->name('barang.hapusriwayat');

// Route::get('/export/exportbarang','ExportLaravelController@export')->name('barang.export');
Route::get('/barcode/{barang}', 'BarangController@barcode')->name('barang.barcode');
Route::get('/barcodeall', 'BarangController@barcodeall')->name('barang.barcodeall');
Route::get('/pdf', 'BarangController@pdf')->name('barang.pdf');
// Route::get('/dp', 'BarangController@dp')->name('barang.dp');
Route::get('/excel/barang', 'BarangController@barangexcel')->name('barang.excel');

Route::get('/report/barang', 'BarangController@report')->name('barang.report');

Route::post('upload', 'BarangController@StoreUploadFile');

Route::middleware('admin')->group(function() {
// Ruoute akun user
Route::get('/user_panel', 'UserPanelController@index')->name('user_panel.index');
Route::get('/user_panel/create', 'UserPanelController@create')->name('user_panel.create');
Route::post('/user_panel/create', 'UserPanelController@store')->name('user_panel.store');
// Route::get('/user_panel/{post}', 'UserPanelController@show')->name('user_panel.show');
Route::get('/user_panel/{user}/edit', 'UserPanelController@edit')->name('user_panel.edit');
Route::patch('/user_panel/{user}/edit', 'UserPanelController@update')->name('user_panel.update');
Route::delete('/user_panel/{user}/delete', 'UserPanelController@destroy')->name('user_panel.destroy');
});});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
